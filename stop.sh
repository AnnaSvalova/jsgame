#!/bin/sh

echo "Killing gunicorn processes..."

sudo pkill -9 gunicorn
sudo pkill -9 frontend 
sudo pkill -9 backend 

echo "Done"
