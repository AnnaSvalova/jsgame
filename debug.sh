#!/bin/bash

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
}

mkdir -p logs

echo "Running gunicorn on 80 port..."
sudo gunicorn --log-file=logs/frontend.log -D -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker" frontend.frontend:app -b localhost:80

sudo tail -f logs/frontend.log

echo "Stoppnig gunicorn..."
sudo pkill -9 gunicorn
sudo pkill -9 frontend 
sudo pkill -9 backend
exit 0
