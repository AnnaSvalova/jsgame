#!/bin/sh

mkdir -p logs
echo "Running frontend on $1 port..."
sudo gunicorn --log-file=logs/backend-$1.log -D -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker" backend.backend:app -b localhost:$1
