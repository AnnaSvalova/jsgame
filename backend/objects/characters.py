import uuid
import random


class Player:
    @classmethod
    def Data(cls):
        return {
            "id": str(uuid.uuid1()),
            "object": "Player",
            "type": "dynamic",
            "polygones": [
                [-5, -5],
                [5, -5],
                [5, 5],
                [5, -5]
            ],
            "heading": 0
        }

    @classmethod
    def create(cls, world, id):
        uData = Player.Data()
        uData["id"] = str(id)
        body = world.CreateDynamicBody(
            position=(random.random() * 800, random.random() * 600),
            allowSleep=False,
            awake=True,
            fixedRotation=True,
            userData=uData
        )
        body.CreatePolygonFixture(
            box=(10, 10),
            density=1,
            friction=0.5
        )
        return body
