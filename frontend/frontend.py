from flask import Flask, request, render_template
from werkzeug import SharedDataMiddleware
import os
import json
from subprocess import call
import gevent

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/help")
def help():
    return render_template("help.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/start")
def game():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        print "Starting backend..."
        call(['./backend/launch-backend.sh', '9099'])
        gevent.sleep(1)
        print "Redirecting user to localhost:9099"
        websocket.send(json.dumps({"command": "redirect", "url": "http://localhost:9099"}))
        return "Ok"
    else:
        return render_template("start.html")


app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
  '/': os.path.join(os.path.dirname(__file__), 'static')
})

if __name__ == '__main__':
    app.run(debug=True)
